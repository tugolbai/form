import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationsComponent } from './applications/applications.component';
import { ApplicationDetailsComponent } from './applications/application-details/application-details.component';
import { EditApplicationComponent } from './applications/edit-application/edit-application.component';
import { ApplicationResolverService } from './applications/application-resolver.service';
import { EmptyApplicationComponent } from './applications/empty-application.component';
import { ThanksComponent } from './applications/thanks.component';

const routes: Routes = [
  {path: '', component: EditApplicationComponent},

  {path: 'applications', component: ApplicationsComponent, children: [
      {path: '', component: EmptyApplicationComponent},
      {
        path: ':id/edit',
        component: EditApplicationComponent,
        resolve: {
          application: ApplicationResolverService
        }
      },
      {
        path: ':id',
        component: ApplicationDetailsComponent,
        resolve: {
          application: ApplicationResolverService
        }
      },
    ]},

  {path: 'thanks', component: ThanksComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
