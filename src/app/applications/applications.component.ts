import { Component, OnDestroy, OnInit } from '@angular/core';
import { Application } from '../shared/application.model';
import { Subscription } from 'rxjs';
import { ApplicationService } from '../shared/application.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit, OnDestroy {
  applications: Application[] = [];
  applicationsChangeSubscription!: Subscription;
  applicationsFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private applicationService: ApplicationService) { }

  ngOnInit(): void {
    this.applications = this.applicationService.getApplications();
    this.applicationsChangeSubscription = this.applicationService.applicationsChange
      .subscribe((applications: Application[]) => {
        this.applications = applications;
      });
    this.applicationsFetchingSubscription = this.applicationService.applicationsFetching
      .subscribe((isFetching: boolean) => {
        this.isFetching = isFetching;
      });
    this.applicationService.fetchApplications();
  }

  ngOnDestroy() {
    this.applicationsChangeSubscription.unsubscribe();
    this.applicationsFetchingSubscription.unsubscribe();
  }
}
