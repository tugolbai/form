import { Component, OnDestroy, OnInit } from '@angular/core';
import { Application } from '../../shared/application.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { ApplicationService } from '../../shared/application.service';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.css']
})
export class ApplicationDetailsComponent implements OnInit, OnDestroy {
  application!: Application;

  isRemoving = false;
  applicationRemovingSubscription!: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private applicationService: ApplicationService,
  ) { }


  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
      this.application = <Application>data.application;
    });

    this.applicationRemovingSubscription = this.applicationService.applicationRemoving
      .subscribe((isRemoving: boolean) => {
        this.isRemoving = isRemoving;
      });
  }

  onRemove() {
    this.applicationService.removeApplication(this.application.id).subscribe(() => {
      this.applicationService.fetchApplications();
      void this.router.navigate(['/applications']);
    });
  }

  ngOnDestroy(): void {
    this.applicationRemovingSubscription.unsubscribe()
  }
}
