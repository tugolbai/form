import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../../shared/application.service';
import { Application } from '../../shared/application.model';
import { phoneNumberValidator } from '../../validate-phoneNumber.directive';

@Component({
  selector: 'app-edit-application',
  templateUrl: './edit-application.component.html',
  styleUrls: ['./edit-application.component.css']
})
export class EditApplicationComponent implements OnInit, OnDestroy{
  applicationForm!: FormGroup;

  isEdit = false;
  editedId = '';
  counter = 0;

  isUploading = false;
  applicationUploadingSubscription!: Subscription;

  constructor(
    private applicationService: ApplicationService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.applicationForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      patronymic: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', [
        Validators.required,
        Validators.maxLength(17),
        phoneNumberValidator
      ]),
      place: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      gender: new FormControl(null, Validators.required),
      comment: new FormControl('', [Validators.required, Validators.maxLength(300)]),
      skills: new FormArray([])
    });

    this.applicationUploadingSubscription = this.applicationService.applicationUploading
      .subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });

    this.route.data.subscribe(data => {
      const application = <Application | null>data.application;
      if (application) {
        this.isEdit = true;
        this.editedId = application.id;
        this.setFormValue({
          firstName: application.firstName,
          lastName: application.lastName,
          patronymic: application.patronymic,
          phoneNumber: application.phoneNumber,
          place: application.place,
          gender: application.gender,
          size: application.size,
          comment: application.comment,
        });
        this.counter = application.comment.length;
        application.skills?.forEach(skill => {
          const skills = <FormArray>this.applicationForm.get('skills');
          const skillGroup = new FormGroup({
            skill: new  FormControl(skill.skill, Validators.required),
            level: new FormControl(skill.level, Validators.required)
          });
          skills.push(skillGroup);
        })
      } else {
        this.counter = 0;
        this.isEdit = false;
        this.editedId = '';
      }
    });
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.applicationForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  skillHasError(fieldName: AbstractControl, errorType: string, skillOrLevel: string) {
    const sk = <FormGroup>fieldName;
    return Boolean(sk && sk.touched && sk.controls[skillOrLevel].errors?.[errorType]);
  }

  addSkill() {
    const skills = <FormArray>this.applicationForm.get('skills');
    const skillGroup = new FormGroup({
      skill: new  FormControl('', Validators.required),
      level: new FormControl('', Validators.required)
    });
    skills.push(skillGroup);
  }

  getSkillControls() {
    const skills =  <FormArray>this.applicationForm.get('skills');
    return skills.controls;
  }

  getLength() {
    const comment = this.applicationForm.get('comment')
    this.counter = comment?.value.length;
  }

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.applicationForm.patchValue(value);
    });
  }

  onSubmit() {
    const id = this.editedId || Math.random().toString();
      const application = new Application(
      id,
      this.applicationForm.get('firstName')?.value,
      this.applicationForm.get('lastName')?.value,
      this.applicationForm.get('patronymic')?.value,
      this.applicationForm.get('phoneNumber')?.value,
      this.applicationForm.get('place')?.value,
      this.applicationForm.get('gender')?.value,
      this.applicationForm.get('size')?.value,
      this.applicationForm.get('comment')?.value,
      this.applicationForm.get('skills')?.value
    );

    const next = () => {
      this.applicationService.fetchApplications();
    };

    if (this.isEdit) {
      this.applicationService.editApplication(application).subscribe(next);
    } else {
      this.applicationService.addApplication(application).subscribe(next);
      void this.router.navigate(['/thanks']);
    }
  }

  resetGender() {
    this.applicationForm.get('gender')?.patchValue('');
  }

  resetSkills() {
    const skills = <FormArray>this.applicationForm.get('skills');
    skills.clear();
  }

  deleteSkill(index: number) {
    const skills = <FormArray>this.applicationForm.get('skills');
    skills.controls.splice(index, 1);
    skills.value.splice(index, 1);
  }

  ngOnDestroy(): void {
    this.applicationUploadingSubscription.unsubscribe();
  }
}
