import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-dish',
  template: `
    <h4>Application details</h4>
    <p>No application is selected</p>
  `
})
export class EmptyApplicationComponent {}
