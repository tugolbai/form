import { Component } from '@angular/core';

@Component({
  selector: 'app-thanks',
  template: `
    <h4>Спасибо, Ваша заявка принята!</h4>
  `
})
export class ThanksComponent {}
