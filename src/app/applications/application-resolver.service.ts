import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Application } from '../shared/application.model';
import { ApplicationService } from '../shared/application.service';

@Injectable({
  providedIn: 'root'
})

export class ApplicationResolverService implements Resolve<Application> {
  constructor(private applicationService: ApplicationService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Application> | Observable<never> {
    const applicationId = <string>route.params['id'];
    return this.applicationService.fetchApplication(applicationId).pipe(mergeMap(application => {
      if (application) {
        return of(application);
      }
      void this.router.navigate(['/application']);
      return EMPTY;
    }));
  }
}
