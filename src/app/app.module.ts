import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ApplicationsComponent } from './applications/applications.component';
import { ApplicationDetailsComponent } from './applications/application-details/application-details.component';
import { EditApplicationComponent } from './applications/edit-application/edit-application.component';
import { ApplicationService } from './shared/application.service';
import { ValidatePhoneNumberDirective } from './validate-phoneNumber.directive';
import { ThanksComponent } from './applications/thanks.component';
import { EmptyApplicationComponent } from './applications/empty-application.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    ApplicationsComponent,
    ApplicationDetailsComponent,
    EditApplicationComponent,
    ValidatePhoneNumberDirective,
    ThanksComponent,
    EmptyApplicationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ApplicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
