export class Application {
  constructor(
    public id: string,
    public firstName: string,
    public lastName: string,
    public patronymic: string,
    public phoneNumber: string,
    public place: string,
    public gender: string,
    public size: string,
    public comment: string,
    public skills: [{[key: string]: string}]
  ) {}
}
