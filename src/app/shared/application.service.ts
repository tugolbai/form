import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Application } from './application.model';

@Injectable()

export class ApplicationService {
  applicationsChange = new Subject<Application[]>();
  applicationsFetching = new Subject<boolean>();
  applicationUploading = new Subject<boolean>();
  applicationRemoving = new Subject<boolean>();

  private applications: Application[] = [];

  constructor(private http: HttpClient) {}

  getApplications() {
    return this.applications.slice();
  }

  fetchApplications() {
    this.applicationsFetching.next(true);
    this.http.get<{ [id: string]: Application }>('https://plovo-57f89-default-rtdb.firebaseio.com/applications.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const applicationData = result[id];
          return new Application(id, applicationData.firstName, applicationData.lastName, applicationData.patronymic,
            applicationData.phoneNumber, applicationData.place, applicationData.gender, applicationData.size,
            applicationData.comment, applicationData.skills);
        });
      }))
      .subscribe(applications => {
        this.applications = applications;
        this.applicationsChange.next(this.applications.slice());
        this.applicationsFetching.next(false);
      }, () => {
        this.applicationsFetching.next(false);
      });
  }

  fetchApplication(id: string) {
    return this.http.get<Application | null>(`https://plovo-57f89-default-rtdb.firebaseio.com/applications/${id}.json`)
      .pipe(map(result => {
        if (!result) {
          return null;
        }
        return new Application(id, result.firstName, result.lastName, result.patronymic,
          result.phoneNumber, result.place, result.gender, result.size,
          result.comment, result.skills);
      }),
    );
  }

  addApplication(application: Application) {
    const body = {
      firstName: application.firstName,
      lastName: application.lastName,
      patronymic: application.patronymic,
      phoneNumber: application.phoneNumber,
      place: application.place,
      gender: application.gender,
      size: application.size,
      comment: application.comment,
      skills: application.skills
    };

    this.applicationUploading.next(true);

    return this.http.post('https://plovo-57f89-default-rtdb.firebaseio.com/applications.json', body).pipe(
      tap(() => {
        this.applicationUploading.next(false);
      }, () => {
        this.applicationUploading.next(false);
      })
    );
  }

  editApplication(application: Application) {
    this.applicationUploading.next(true);

    const body = {
      firstName: application.firstName,
      lastName: application.lastName,
      patronymic: application.patronymic,
      phoneNumber: application.phoneNumber,
      place: application.place,
      gender: application.gender,
      size: application.size,
      comment: application.comment,
      skills: application.skills
    };

    return this.http.put(`https://plovo-57f89-default-rtdb.firebaseio.com/applications/${application.id}.json`, body)
      .pipe(tap(() => {
          this.applicationUploading.next(false);
        }, () => {
          this.applicationUploading.next(false);
        })
    );
  }

  removeApplication(id: string) {
    this.applicationRemoving.next(true);

    return this.http.delete(`https://plovo-57f89-default-rtdb.firebaseio.com/applications/${id}.json`).pipe(
      tap(() => {
        this.applicationRemoving.next(false);
      }, () => {
        this.applicationRemoving.next(false);
      })
    );
  }
}
