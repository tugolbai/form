import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

export const  phoneNumberValidator =  (control: AbstractControl): ValidationErrors | null => {
  const hasNumber = /^\(?\+([9]{2}?[6])\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/.test(control.value);
  if (hasNumber) {
    return null;
  }
  return {phoneNumber: true};
};

@Directive({
  selector: '[appPhoneNumber]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatePhoneNumberDirective,
    multi: true
  }]
})

export class ValidatePhoneNumberDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneNumberValidator(control);
  }
}
